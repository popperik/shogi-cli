"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EMPTY = exports.BLUE = exports.RED = void 0;
exports.RED = { name: 'red', value: 31 };
exports.BLUE = { name: 'blue', value: 34 };
exports.EMPTY = { name: 'empty', value: 10 };
